---
title: Talent Acquisition Performance Indicators
description: "This page is an overview of the performance indicators we use to meausre success of the talent acquisition team."
---

{{< performance-indicators "talent_acquisition" >}}
